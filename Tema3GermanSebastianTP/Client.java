package dataAccessLayer;

public class Client {
	private String nume;
	private int id;
	private int suma;
	
	public Client(String nume, int id, int suma){
		this.nume=nume;
		this.id=id;
		this.suma=suma;
	}
	public Client(){}
	
	public String getNume(){
		return this.nume;
	}
	public int getId(){
		return this.id;
	}
	
	public void setNume(String nume){
		this.nume=nume;
	}
	public int getSuma(){
		return this.suma;
	}
	
	public void retrage(int nr){
		if(nr<=this.suma){
			suma-=nr;
		}
		else System.out.println("suma indisponibila");
	}
}
