package dataAccessLayer;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Comanda {
	private int id;
	private String numeClient;
	private String numeProdus;
	private int cantitate;
	//private int pretTotal;
	
	//private static int nrComanda=0;
	
	public Comanda(int id,String numeClient,  String numeProdus, int cantitate){
		this.id=id;
		this.numeClient=numeClient;
		this.numeProdus=numeProdus;
		this.cantitate=cantitate;
		//this.nrComanda++;
	}
	public int getId(){
		return this.id;
	}
	public String getNumeClient(){
		return this.numeClient;
	}
	public String getNumeProdus(){
		return this.numeProdus;
	}
	public int getCantitate(){
		return this.cantitate;
	}
	
	public int validareComanda(){
		 ConnectionFactory con=ConnectionFactory.getInstance();
		ArrayList<Client> arrClient=new ArrayList<>();
		 ArrayList<Produs> arrProdus=new ArrayList<>();
		// ViewClient c=new ViewClient("nimic");
		 //ViewProdus p=new ViewProdus("nimic");
		
		 String sql="select * from clienti";
		try{
		   con.myRs=con.myStmt.executeQuery(sql);
		   Client client;
		   while(con.myRs.next()){
			   client=new Client(con.myRs.getString("nume"),con.myRs.getInt("id"),con.myRs.getInt("suma"));
			   arrClient.add(client);
		   }
		}catch (Exception e){
			e.printStackTrace();
		}
		
		/*for(int i=0; i<arrClient.size(); i++)
			System.out.println(arrClient.get(i).getNume());
		
		System.out.println("muie");
		*/
		String sql2="select * from produse";
		try{
		   con.myRs=con.myStmt.executeQuery(sql2);
		   Produs produs;
		   while(con.myRs.next()){
			   produs=new Produs(con.myRs.getString("nume"),con.myRs.getInt("cantitate"),con.myRs.getInt("pret"));
			   arrProdus.add(produs);
		   }
		}catch (Exception e){
			e.printStackTrace();
		}
		
		
		int gc=0,gp=0,poz1=0,poz2=0;
		for(int i=0; i<arrClient.size(); i++)
			if(arrClient.get(i).getNume().equals(this.numeClient)){
				gc=1;
				poz1=i;
				System.out.println(this.numeClient);
			}
		for(int i=0; i<arrProdus.size(); i++)
			if(arrProdus.get(i).getNume().equals(this.numeProdus)){
				gp=1;
				poz2=i;
				System.out.println("nume produs:  "+this.numeProdus);
			}
		
		int pretTotal_1=arrProdus.get(poz2).getPret()*this.cantitate;
		int validPret=0;
		System.out.println(arrClient.get(poz1).getSuma());
		if(arrClient.get(poz1).getSuma()>=pretTotal_1 && arrProdus.get(poz2).getCantitate()>=this.cantitate && gc==1 && gp==1){
			int consumat=arrClient.get(poz1).getSuma()-pretTotal_1;
			validPret=1;
			String sql3="update clienti set suma='"+consumat
					+"' where "
					+"nume='"+arrClient.get(poz1).getNume()+"'";
			
			try{
				int editat=con.myStmt.executeUpdate(sql3);
				//System.out.println(editat);
			}catch (Exception e){
				e.printStackTrace();
			}
			int consumat2=arrProdus.get(poz2).getCantitate()-this.cantitate;
			String sql4="update produse set cantitate='"+consumat2
					+"' where "
					+"nume='"+arrProdus.get(poz2).getNume()+"'";
			
			try{
				int editat=con.myStmt.executeUpdate(sql4);
				//System.out.println(editat);
			}catch (Exception e){
				e.printStackTrace();
			}
			
		
		}
		
		if(gc==1 && gp==1 && validPret==1){
			
			//System.out.println("comanda valida");
			return 1;
		}
		else{
			//System.out.println("comanda invalida");
			JOptionPane.showMessageDialog(null, "Comanda ne-efectuata. Cantitate sau pret indisponibile");
		    return 0;
		}
		
		
		
		
	}
	
}
