package dataAccessLayer;
import java.sql.*;
import java.util.logging.Logger;

public class ConnectionFactory {
	private static final String DRIVER="com.mysql.jdbc.Driver";
	private static final Logger LOGGER=Logger.getLogger(ConnectionFactory.class.getName());
	private static final String DBURL="jdbc:mysql://localhost:3306/schooldb?useSSL=false";
	private static final String USER="root";
	private static final String PASS="sql2405";
	
	public Connection myConn;
	public Statement myStmt;
	public ResultSet myRs;
	public PreparedStatement prStmt;
	
	private static ConnectionFactory singleInstance=new ConnectionFactory();
	
	
	private ConnectionFactory(){
		try{
			Class.forName(DRIVER);
		}catch (ClassNotFoundException e){
			e.printStackTrace();
		}
		
		try{
		    myConn=DriverManager.getConnection(DBURL,USER,PASS);
		    
		}catch (Exception e){
			e.printStackTrace();
		}
		
		try{
			myStmt=myConn.createStatement();
		
		}catch (Exception e){
			e.printStackTrace();
		}
		
		
	}
	
	public static ConnectionFactory getInstance(){
		return singleInstance;
	}
	
	/*private Connection createConnection() {
		try{
		    myConn=DriverManager.getConnection(DBURL,USER,PASS);
		}catch (Exception e){
			e.printStackTrace();
		}
		return myConn;
	}
	*/
}
