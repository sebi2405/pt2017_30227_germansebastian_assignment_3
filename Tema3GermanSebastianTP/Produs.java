package dataAccessLayer;

public class Produs {
	private String nume;
	private int cantitate;
	private int pretPerUnitate;
	
	public Produs(String nume,int cantitate,int pretPerUnitate){
		this.nume=nume;
		this.cantitate=cantitate;
		this.pretPerUnitate=pretPerUnitate;
	}
	public Produs(){}
	
	public String getNume(){
		return this.nume;
	}
	public int getCantitate(){
		return this.cantitate;
	}
	public int getPret(){
		return this.pretPerUnitate;
	}
	public void scadeCantitate(int cant){
		if(cant<this.cantitate){
			this.cantitate-=cant;
		}
		else System.out.println("Cantitate indisponibila");
	}
}
