package dataAccessLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class ViewClient implements ActionListener{
	private JFrame panou=new JFrame("Operatii clienti");
	private ConnectionFactory con=ConnectionFactory.getInstance();
	
	private ArrayList<Client> array=new ArrayList<>();
	private JScrollPane js;
	private JTable jTable_display;
	private JButton adaugare=new JButton("Adauga Client");
	
	private JLabel nume=new JLabel("Nume:");
	private JLabel id=new JLabel("Id:");
	private JLabel suma=new JLabel("Suma:");
	private JTextField numeNou=new JTextField();
	private JTextField idNou=new JTextField();
	private JTextField sumaNou=new JTextField();
	
	private JLabel numeEdit=new JLabel("Nume:");
	private JLabel sumaEdit=new JLabel("Noua suma:");
	private JTextField numeEdit2=new JTextField();
	private JTextField sumaEdit2=new JTextField();
	private JButton editare=new JButton("Editare client");
	
	private JLabel stergeNume=new JLabel("Nume:");
	private JTextField stergeNume2=new JTextField();
	private JButton sterge=new JButton("Sterge Client");
	
	private JButton menu=new JButton("Inapoi la meniu");
	
	public ViewClient() {
		panou.setSize(520, 340);
		panou.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panou.setVisible(true);
		panou.setLayout(null);
		
		/*js=new JScrollPane();
		js.setBounds(100,20,200,100);
		panou.add(js);

		
		
		String sql="select * from clienti";
		try{
		   con.myRs=con.myStmt.executeQuery(sql);
		   Client client;
		   while(con.myRs.next()){
			   client=new Client(con.myRs.getString("nume"),con.myRs.getInt("id"));
			   array.add(client);
		   }
		}catch (Exception e){
			e.printStackTrace();
		}
		Object [][]row=new Object[10][2];
		for(int i=0; i<array.size(); i++){
			row[i][0]=array.get(i).getId();
			row[i][1]=array.get(i).getNume();
		}
		String[]coloane={"Id","Nume"};
        jTable_display=new JTable(row,coloane);
        js.setViewportView(jTable_display);
        */
		
		id.setBounds(10,150,50,20);
		panou.add(id);
		nume.setBounds(10, 180, 50, 20);
		panou.add(nume);
		suma.setBounds(10, 210, 50, 20);
		panou.add(suma);
		idNou.setBounds(60, 150, 60, 20);
		panou.add(idNou);
		numeNou.setBounds(60, 180, 60, 20);
		panou.add(numeNou);
		sumaNou.setBounds(60, 210, 60, 20);
		panou.add(sumaNou);
		
		numeEdit.setBounds(140, 150, 80, 20);
		panou.add(numeEdit);
		sumaEdit.setBounds(140, 180, 80, 20);
		panou.add(sumaEdit);
		numeEdit2.setBounds(230, 150, 60, 20);
		panou.add(numeEdit2);
		sumaEdit2.setBounds(230, 180, 60, 20);
	    panou.add(sumaEdit2);
	    
	    stergeNume.setBounds(330, 150, 60, 20);
	    panou.add(stergeNume);
	    stergeNume2.setBounds(400, 150, 60, 20);
	    panou.add(stergeNume2);
		
	    menu.setBounds(150, 270, 150, 20);
	    panou.add(menu);
		showClienti();
		
        adaugare.setBounds(5, 240, 120, 20);
        adaugare.setVisible(true);
        editare.setBounds(160, 240, 120, 20);
        sterge.setBounds(340, 240, 120, 20);
        panou.add(sterge);
        panou.add(editare);
        panou.add(adaugare);
        editare.addActionListener(this);
        adaugare.addActionListener(this);
        sterge.addActionListener(this);
        menu.addActionListener(this);
	}
    public ViewClient(String a){
    	showClienti();
        
    }
	
	
	public void showClienti(){
		
		js=new JScrollPane();
		js.setBounds(160,20,200,100);
		panou.add(js);

		
		
		String sql="select * from clienti";
		try{
		   con.myRs=con.myStmt.executeQuery(sql);
		   Client client;
		   while(con.myRs.next()){
			   client=new Client(con.myRs.getString("nume"),con.myRs.getInt("id"),con.myRs.getInt("suma"));
			   array.add(client);
		   }
		}catch (Exception e){
			e.printStackTrace();
		}
		Object [][]row=new Object[10][3];
		for(int i=0; i<array.size(); i++){
			row[i][0]=array.get(i).getId();
			row[i][1]=array.get(i).getNume();
			row[i][2]=array.get(i).getSuma();
		}
		String[]coloane={"Id","Nume","Suma"};
        jTable_display=new JTable(row,coloane);
        js.setViewportView(jTable_display);
        
        
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		if(ev.getSource()==adaugare){
		
			String sql="insert into clienti values('"+Integer.parseInt(idNou.getText())
			+"','"+numeNou.getText()+"','"+Integer.parseInt(sumaNou.getText())+"')";
			
			try{
				int adaugat=con.myStmt.executeUpdate(sql);
				System.out.println(adaugat);
			}catch (Exception e){
				e.printStackTrace();
			}
			
			this.panou.setVisible(false);
			ViewClient v=new ViewClient();
			System.gc();
			
		}
		else if(ev.getSource()==editare){
			String sql="update clienti set suma='"+Integer.parseInt(sumaEdit2.getText())+"' where "
					+"nume='"+numeEdit2.getText()+"'";
			
			try{
				int editat=con.myStmt.executeUpdate(sql);
				System.out.println(editat);
			}catch (Exception e){
				e.printStackTrace();
			}
			
			this.panou.setVisible(false);
			ViewClient v=new ViewClient();
			System.gc();
		}
		else if(ev.getSource()==sterge){
			String sql="delete from clienti where nume='"+stergeNume2.getText()+"'";
			
			try{
				int sters=con.myStmt.executeUpdate(sql);
				System.out.println(sters);
			}catch (Exception e){
				e.printStackTrace();
			}
		
			this.panou.setVisible(false);
			ViewClient v=new ViewClient();
			System.gc();
		}
		else if(ev.getSource()==menu){
			this.panou.setVisible(false);
			ViewMeniu m=new ViewMeniu();
		}
		
	}
	public ArrayList<Client> getArray(){
		return array;
	}
	
}
