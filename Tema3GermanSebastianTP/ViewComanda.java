package dataAccessLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.log.SysoCounter;
import com.itextpdf.text.pdf.PdfWriter;

public class ViewComanda implements ActionListener {
	private JFrame panou=new JFrame("Operatii comenzi");
	private ConnectionFactory con=ConnectionFactory.getInstance();
	
	private ArrayList<Comanda> array=new ArrayList<>();
	private JScrollPane js;
	private JTable jTable_display;
	private JButton adaugare=new JButton("Adauga Comanda");
	
	private JLabel id=new JLabel("Id:");
	private JLabel numeClient=new JLabel("Nume Client:");
	private JLabel numeProdus=new JLabel("Nume Produs:");
	private JLabel cantitate=new JLabel("Cantitate:");
	private JTextField idd=new JTextField();
	private JTextField numeCl=new JTextField();
	private JTextField numePr=new JTextField();
	private JTextField cant=new JTextField();

	private JLabel idSterge=new JLabel("Id:");
	private JTextField idSters=new JTextField();
	private JButton sterge=new JButton("Sterge Comanda");
	
	private JLabel tip=new JLabel("Id Comanda:");
	private JTextField tipar=new JTextField();
	private JButton tipareste=new JButton("Tipareste Comanda");
	
	private JButton back=new JButton("Inapoi la meniu");
	
	public ViewComanda(){
		panou.setSize(520, 340);
		panou.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panou.setVisible(true);
		panou.setLayout(null);
		
		id.setBounds(10, 130, 150, 20);
		panou.add(id);
		idd.setBounds(95, 130, 60, 20);
		panou.add(idd);
		numeClient.setBounds(10,160,85,20);
		panou.add(numeClient);
		numeProdus.setBounds(10, 190, 85, 20);
		panou.add(numeProdus);
		cantitate.setBounds(10, 220, 85, 20);
		panou.add(cantitate);
		numeCl.setBounds(95, 160, 60, 20);
		panou.add(numeCl);
		numePr.setBounds(95, 190, 60, 20);
		panou.add(numePr);
		cant.setBounds(95, 220, 60, 20);
		panou.add(cant);
		
		idSterge.setBounds(170, 220, 30, 20);
		panou.add(idSterge);
		idSters.setBounds(210, 220, 60, 20);
		panou.add(idSters);
		sterge.setBounds(160, 250, 140, 20);
		panou.add(sterge);
		
		tip.setBounds(310,220,90,20);
		panou.add(tip);
		tipar.setBounds(390,220,60,20);
		panou.add(tipar);
		tipareste.setBounds(310, 250, 150, 20);
		panou.add(tipareste);
		
		back.setBounds(155, 280, 150, 20);
		panou.add(back);
		
		showComenzi();
		
		tipareste.addActionListener(this);
		back.addActionListener(this);
        sterge.addActionListener(this);
		adaugare.setBounds(10, 250,140,20);
		panou.add(adaugare);
		adaugare.addActionListener(this);
	}
	
public void showComenzi(){
		

	js=new JScrollPane();
	js.setBounds(70,20,400,100);
	panou.add(js);

	
	
	String sql="select * from comenzi";
	try{
	   con.myRs=con.myStmt.executeQuery(sql);
	   Comanda comanda;
	   while(con.myRs.next()){
		   comanda=new Comanda(con.myRs.getInt("nrComanda"),con.myRs.getString("numeClient"),con.myRs.getString("numeProdus"),con.myRs.getInt("cantitate"));
		   array.add(comanda);
	   }
	}catch (Exception e){
		e.printStackTrace();
	}
	Object [][]row=new Object[10][4];
	for(int i=0; i<array.size(); i++){
		row[i][0]=array.get(i).getId();
		row[i][1]=array.get(i).getNumeClient();
		row[i][2]=array.get(i).getNumeProdus();
		row[i][3]=array.get(i).getCantitate();
	}
	String[]coloane={"Numar comanda","Client","Produs","Cantitate"};
    jTable_display=new JTable(row,coloane);
    js.setViewportView(jTable_display);
        
        
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		// TODO Auto-generated method stub
		if(ev.getSource()==adaugare){
			
			Comanda c=new Comanda(Integer.parseInt(idd.getText()),numeCl.getText(),numePr.getText(),Integer.parseInt(cant.getText()));
			if(c.validareComanda()==1){
				//JOptionPane.showMessageDialog(null, "Comanda efectuata cu succes");
			
			
			String sql="insert into comenzi values('"+Integer.parseInt(idd.getText())
			+"','"+numeCl.getText()+"','"+numePr.getText()+"','"
			+Integer.parseInt(cant.getText())+"')";
			
			try{
				int adaugat=con.myStmt.executeUpdate(sql);
				System.out.println(adaugat);
			}catch (Exception e){
				e.printStackTrace();
			}
			}
			
			
			this.panou.setVisible(false);
			ViewComanda v=new ViewComanda();
			System.gc();
			
		}
		else if(ev.getSource()==sterge){
			
			String sql="delete from comenzi where nrComanda='"+Integer.parseInt(idSters.getText())+"'";
			
			try{
				int sters=con.myStmt.executeUpdate(sql);
				System.out.println(sters);
			}catch (Exception e){
				e.printStackTrace();
			}
		
			this.panou.setVisible(false);
			ViewComanda v=new ViewComanda();
			System.gc();
		}
		else if(ev.getSource()==back){
			this.panou.setVisible(false);
			ViewMeniu m=new ViewMeniu();
		}
		else if(ev.getSource()==tipareste){
			String name="",prod="";
			int cant=0;
			int pretul=0;
			int gasit=0;
			String sql="select * from comenzi";
			try{
			   con.myRs=con.myStmt.executeQuery(sql);
			   Comanda comanda;
			   while(con.myRs.next()){
				   comanda=new Comanda(con.myRs.getInt("nrComanda"),con.myRs.getString("numeClient"),con.myRs.getString("numeProdus"),con.myRs.getInt("cantitate"));
				   
				   if(comanda.getId()==Integer.parseInt(tipar.getText())){
					    gasit=1;
					    name=comanda.getNumeClient();
					    prod=comanda.getNumeProdus();
					    cant=comanda.getCantitate();
				   }
			   }
			 }catch(Exception e){
				   e.printStackTrace();
			   }
			if(gasit==1){
				String sql2="select * from produse";
				try{
				   con.myRs=con.myStmt.executeQuery(sql2);
				   while(con.myRs.next()){
					   Produs produs=new Produs(con.myRs.getString("nume"),con.myRs.getInt("cantitate"),con.myRs.getInt("pret"));
					  // System.out.println(produs.getNume());
					   if(produs.getNume().equals(prod)){
						   pretul=produs.getPret();
						   
					   }
				   }
				 }catch(Exception e){
					   e.printStackTrace();
				   }
			}
			
			if(gasit==1){
				Document document=new Document(PageSize.A4);
				document.addAuthor("Sebastian");
				document.addTitle("Comanda nr. "+tipar.getText());
				try{
					PdfWriter.getInstance(document, new FileOutputStream("Comanda"+tipar.getText()+".pdf"));
					document.open();
					document.add(new Paragraph("                                                  Comanda nr."+tipar.getText()));
					Paragraph p1=new Paragraph("\n\n\nNume client:     "+name);
					document.add(p1);
					Paragraph p2=new Paragraph("Nume produs:  "+prod );
					document.add(p2);
					Paragraph p3=new Paragraph("Cantitate:         "+cant);
					document.add(p3);
					Paragraph p4=new Paragraph("Pret bucata:     "+pretul);
					document.add(p4);
					Paragraph p5=new Paragraph("Pret total:         "+pretul*cant);
					document.add(p5);
				
					JOptionPane.showMessageDialog(null, "Comanda tiparita cu succes");
					tipar.setText("");
				
				}catch (Exception e){
					e.printStackTrace();
				}
				document.close();
			}
			
		
	       
		else
			JOptionPane.showMessageDialog(null, "Nr. comanda inexistent");
	        tipar.setText("");
		}
	}
}

