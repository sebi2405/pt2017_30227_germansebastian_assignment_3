package dataAccessLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class ViewMeniu implements ActionListener {
	private JFrame panou=new JFrame("Meniu");
	private JButton clienti=new JButton("Clienti");
	private JButton produse=new JButton("Produse");
	private JButton comenzi=new JButton("Comenzi");
	
	public ViewMeniu(){
		panou.setSize(350, 200);
		panou.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panou.setLayout(null);
		panou.setVisible(true);
		
		clienti.setBounds(10, 50, 80, 20);
		panou.add(clienti);
		clienti.addActionListener(this);
		
		produse.setBounds(100,50,100,20);
		panou.add(produse);
		produse.addActionListener(this);
		
		comenzi.setBounds(210, 50, 100, 20);
		panou.add(comenzi);
		comenzi.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		if(ev.getSource()==clienti){
			this.panou.setVisible(false);
			ViewClient v=new ViewClient();
		}
		else if(ev.getSource()==produse){
			this.panou.setVisible(false);
			ViewProdus p=new ViewProdus();
		}
		else if(ev.getSource()==comenzi){
			this.panou.setVisible(false);
			ViewComanda c=new ViewComanda();
		}
		
	}
}
