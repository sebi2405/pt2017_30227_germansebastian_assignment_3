package dataAccessLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class ViewProdus implements ActionListener{
	private JFrame panou=new JFrame("Operatii produse");
	private ConnectionFactory con=ConnectionFactory.getInstance();
	
	private ArrayList<Produs> array=new ArrayList<>();
	private JScrollPane js;
	private JTable jTable_display;
	private JButton adaugare=new JButton("Adauga Produs");
	
	private JLabel nume=new JLabel("Nume:");
	private JLabel cantitate=new JLabel("Cantitate:");
	private JLabel pret=new JLabel("Pret:");
	private JTextField numeNou=new JTextField();
	private JTextField cantNou=new JTextField();
	private JTextField pretNou=new JTextField();
	
	private JLabel numeEdit=new JLabel("Nume:");
	private JLabel pretEdit=new JLabel("Noul pret:");
	private JTextField numeEdit2=new JTextField();
	private JTextField pretEdit2=new JTextField();
	private JButton editare=new JButton("Editare Produs");
	
	private JLabel stergeNume=new JLabel("Nume:");
	private JTextField stergeNume2=new JTextField();
	private JButton sterge=new JButton("Sterge Produs");
	
	private JButton menu=new JButton("Inapoi la meniu");
	
	public ViewProdus() {
		panou.setSize(520, 340);
		panou.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		panou.setVisible(true);
		panou.setLayout(null);
		
		/*js=new JScrollPane();
		js.setBounds(100,20,200,100);
		panou.add(js);

		
		
		String sql="select * from clienti";
		try{
		   con.myRs=con.myStmt.executeQuery(sql);
		   Client client;
		   while(con.myRs.next()){
			   client=new Client(con.myRs.getString("nume"),con.myRs.getInt("id"));
			   array.add(client);
		   }
		}catch (Exception e){
			e.printStackTrace();
		}
		Object [][]row=new Object[10][2];
		for(int i=0; i<array.size(); i++){
			row[i][0]=array.get(i).getId();
			row[i][1]=array.get(i).getNume();
		}
		String[]coloane={"Id","Nume"};
        jTable_display=new JTable(row,coloane);
        js.setViewportView(jTable_display);
        */
		
		nume.setBounds(10,150,70,20);
		panou.add(nume);
		cantitate.setBounds(10, 180, 70, 20);
		panou.add(cantitate);
		pret.setBounds(10, 210, 70, 20);
		panou.add(pret);
		numeNou.setBounds(70, 150, 60, 20);
		panou.add(numeNou);
		cantNou.setBounds(70, 180, 60, 20);
		panou.add(cantNou);
		pretNou.setBounds(70, 210, 60, 20);
		panou.add(pretNou);
		
		numeEdit.setBounds(150, 150, 80, 20);
		panou.add(numeEdit);
		pretEdit.setBounds(150, 180, 80, 20);
		panou.add(pretEdit);
		numeEdit2.setBounds(230, 150, 60, 20);
		panou.add(numeEdit2);
		pretEdit2.setBounds(230, 180, 60, 20);
	    panou.add(pretEdit2);
	    
	    stergeNume.setBounds(330, 150, 60, 20);
	    panou.add(stergeNume);
	    stergeNume2.setBounds(400, 150, 60, 20);
	    panou.add(stergeNume2);
		
	    menu.setBounds(150, 270, 150, 20);
	    panou.add(menu);
		
	    showProduse();
		
        adaugare.setBounds(5, 240, 140, 20);
        adaugare.setVisible(true);
        editare.setBounds(160, 240, 120, 20);
        sterge.setBounds(340, 240, 120, 20);
        panou.add(sterge);
        panou.add(editare);
        panou.add(adaugare);
        editare.addActionListener(this);
        adaugare.addActionListener(this);
        sterge.addActionListener(this);
        menu.addActionListener(this);
	}
    public ViewProdus(String a){
    	
    }
	
	
	public void showProduse(){
		
		js=new JScrollPane();
		js.setBounds(160,20,200,100);
		panou.add(js);

		
		
		String sql="select * from produse";
		try{
		   con.myRs=con.myStmt.executeQuery(sql);
		   Produs produs;
		   while(con.myRs.next()){
			   produs=new Produs(con.myRs.getString("nume"),con.myRs.getInt("cantitate"),con.myRs.getInt("pret"));
			   array.add(produs);
		   }
		}catch (Exception e){
			e.printStackTrace();
		}
		Object [][]row=new Object[10][3];
		for(int i=0; i<array.size(); i++){
			row[i][0]=array.get(i).getNume();
			row[i][1]=array.get(i).getCantitate();
			row[i][2]=array.get(i).getPret();
		}
		String[]coloane={"Nume","Cantiate","Pret/Unitate"};
        jTable_display=new JTable(row,coloane);
        js.setViewportView(jTable_display);
        
        
	}
	
	@Override
	public void actionPerformed(ActionEvent ev) {
		if(ev.getSource()==adaugare){
		
			String sql="insert into produse values('"+numeNou.getText()
			+"','"+Integer.parseInt(cantNou.getText())+"','"+Integer.parseInt(pretNou.getText())+"')";
			
			try{
				int adaugat=con.myStmt.executeUpdate(sql);
				System.out.println(adaugat);
			}catch (Exception e){
				e.printStackTrace();
			}
			
			this.panou.setVisible(false);
			ViewProdus v=new ViewProdus();
			System.gc();
			
		}
		else if(ev.getSource()==editare){
			String sql="update produse set pret='"+Integer.parseInt(pretEdit2.getText())+"' where "
					+"nume='"+numeEdit2.getText()+"'";
			
			try{
				int editat=con.myStmt.executeUpdate(sql);
				System.out.println(editat);
			}catch (Exception e){
				e.printStackTrace();
			}
			
			this.panou.setVisible(false);
			ViewProdus v=new ViewProdus();
			System.gc();
		}
		else if(ev.getSource()==sterge){
			String sql="delete from produse where nume='"+stergeNume2.getText()+"'";
			
			try{
				int sters=con.myStmt.executeUpdate(sql);
				System.out.println(sters);
			}catch (Exception e){
				e.printStackTrace();
			}
		
			this.panou.setVisible(false);
			ViewProdus v=new ViewProdus();
			System.gc();
		}
		else if(ev.getSource()==menu){
			this.panou.setVisible(false);
			ViewMeniu m=new ViewMeniu();
		}
		
		
	}
	public ArrayList<Produs> getArray(){
		return array;
	}
	
}
